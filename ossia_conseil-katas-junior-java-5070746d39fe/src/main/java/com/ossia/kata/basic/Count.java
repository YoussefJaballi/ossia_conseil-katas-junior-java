package com.ossia.kata.basic;

import java.util.HashMap;
import java.util.Map;

public class Count {
    public static int countOccurrences(String sentence, String target) {
        int count = 0;
        int index = sentence.indexOf(target);
        while (index != -1) {
            count++;
            index = sentence.indexOf(target, index + 1);
        }
        return count;
    }
    
    
    public static Map<String, Integer> countMots(String phrase) {
        Map<String, Integer> wordCounts = new HashMap<>();
        
        String[] words = phrase.split("\\s+");

        for (String word : words) {
            word = word.replaceAll("[^a-zA-Z]", "").toLowerCase();

            if (wordCounts.containsKey(word)) {
                wordCounts.put(word, wordCounts.get(word) + 1);
            } else {
                wordCounts.put(word, 1);
            }
        }
        
        return wordCounts;
    }

    public static void main(String[] args) {
        String sentence = "This is a test sentence. This sentence is a test.";
        String target = "is";
        int occurrences = countOccurrences(sentence, target);
        System.out.println("Occurrences of '" + target + "': " + occurrences);
        System.out.println(countMots(sentence));
    }
}
