package com.ossia.kata.basic;

import java.util.ArrayList;
import java.util.List;

public class CompileError {

    interface IHello {
        String hello();
    }

    class Hello implements IHello {

        private String name;

        public Hello(String name) {
            this.name = name;
        }

        @Override
        public String hello() {
            return "Hello "+name;
        }
    }

    public void sayHello() {
    	List<Hello> l = new ArrayList<>();
    	l.add(new Hello("Ossia1"));
    	l.add(new Hello("Ossia2"));
    	l.forEach(o -> o.hello());
    }

}
