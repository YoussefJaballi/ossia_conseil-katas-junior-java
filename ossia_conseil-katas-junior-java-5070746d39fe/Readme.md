# Katas pour profil Junior
## Durée : 30min
## Démarche à suivre pour tous les katas
- ### TDD
- ### Tester et optimiser le plus possible

# 1. FizzBuzz

Le kata original est [ici](https://codingdojo.org/kata/FizzBuzz/)

**But de l'exercice** : on veut écrire un programme qui prend en entrée un nombre et qui retourne une chaîne de
caractères en respectant les règles suivantes :

1. Si le nombre est mutiple de 3 et de 5, on doit retourner FizzBuzz
2. Si le nombre est seulement multiple de 3, on doit retourner Fizz
3. Si le nombre est seulement multiple de 5, on doit retourner Buzz
4. Dans tous les autres cas, on retourne le nombre sous forme de chaine de caractères


# 2. Count

- **Partie 1** : Compter le nombre d’occurrences d'une chaîne de caractère dans une chaîne de caractères
    - *Paramètres d'entrée* : une phrase et une chaîne de caractères
    - *En sortie* : le nombre d'occurences de la chaîne dans la phrase


- **Partie 2** : On souhaite compter le nombre d’occurrences de chaque mot dans une phrase
    - *Paramètres d'entrée* : une phrase
    - *En sortie* : une structure de données permettant de compter pour chaque mot de la phrase, son nombre d'occurences
      dans la phrase